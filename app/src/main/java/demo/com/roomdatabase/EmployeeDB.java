package demo.com.roomdatabase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by Hardik Lakhani @Yudiz on 06/04/18.
 */

@Database(entities = {Employee.class}, version = 1)
public abstract class EmployeeDB extends RoomDatabase {
    public abstract EmployeeDao employeeDao();
}
